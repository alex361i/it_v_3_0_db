/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mbs;

/**
 *
 * @author yo
 */
//curent acount that charges after a number of transactions
 public class CurrentAcc extends BankAcc
 {  
    
    public CurrentAcc(double iniBalance)
   {  
      // calling the superclass constructor
       super(iniBalance);
       
       // the counter for transactions
       trCount = 0; 
    }
 
    public void deposit(double amount) 
    {  
       trCount++;
       // add to balance from super class(bankAcc
       super.deposit(amount); 
    }
    
    public void withdraw(double amount) 
    {  
       trCount++;
       // substract from balance from super class(bankAcc 
       super.withdraw(amount); 
    }
 
   
    public void deductCost()
    {  
       if (trCount > GRATIS_TRAN)
       {  
          double charge = TRAN_COST *(trCount - GRATIS_TRAN);
          super.withdraw(charge);
       }
       //reset for transactions counter
       trCount = 0;
    }
 
    private int trCount;
    //class constants for the currentAcc
    private static final int GRATIS_TRAN = 3;
    private static final double TRAN_COST = 5.0;
 }
