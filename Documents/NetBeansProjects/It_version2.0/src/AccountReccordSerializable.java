
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yo
 */
public class AccountReccordSerializable implements Serializable {
    private int account;
    
    private String firstName;
    private String lastName;
    private double balance;
    
    public AccountReccordSerializable() {
        this(0, "", "", 0.0);
    }
    public AccountReccordSerializable(int acc, String first ,String last,double bal){
        setAccout(acc);
        setFirstName(first);
        setLastName(last);
        setBalance(bal);
        
    }
    public void setAccout(int acc){
        account=acc;
    
    }
    
    public int getAccount(){
        return account;
    }
    public void setFirstName(String first){
        firstName=first;
    }
    public String getFirstName(){
        return firstName;
    }
    
    public void setLastName(String last){
        lastName=last;
    }
    public String getLastName(){
        return lastName;
    }
    public void setBalance(double bal){
        balance=bal;
    }
    public double getBalance(){
        return balance;
    }
    
}
