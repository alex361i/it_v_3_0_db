
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yo
 */
public class Issue implements Serializable{
    private String subject;
    private int priority;
    private int status;
    private String comment;
    
    public Issue(){
        this("",0,0,"");
    }
    public Issue(String subject, int priority, int status, String comment){
        setSubject(subject);
        setPriority(priority);
        setStatus(status);
        setComment(comment);
        
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
}

