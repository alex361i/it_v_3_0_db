/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.classes;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author yo
 */
public class ConnectToMysql {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException,ClassNotFoundException {
        // TODO code application logic here
        Connection conn=DBConnector.getConnection();
        
        String sqlRead="SELECT * FROM issue;";
        //String sqlWrite="INSERT INTO issue (subject, priority, status','comment')"+
//"VALUES ('Tom B. Erichsen','Skagen 21','Stavanger','something'); ";
        
        Statement stmt=conn.createStatement();
        stmt.execute(sqlRead);
        ResultSet rs=stmt.getResultSet();
        
//        Statement stmtW=conn.createStatement();
//        stmtW.execute(sqlWrite);
        
        
        while(rs.next()){
            //int id=rs.getInt("idissue");
            String subject=rs.getString("subject");
            String priority=rs.getString("priority");
            String status=rs.getString("status");
            String comment=rs.getString("comment");
            
            System.out.println(" subject="+ subject + " priority="+ priority +" status="+status+" comment= "+comment);
            
        }
    }
    
}
