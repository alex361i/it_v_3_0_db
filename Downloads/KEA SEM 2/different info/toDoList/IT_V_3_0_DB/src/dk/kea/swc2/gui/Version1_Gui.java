/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dk.kea.swc2.gui;
import javax.swing.UIManager.*;
import dk.kea.swc2.classes.DBConnector;
import dk.kea.swc2.classes.Issue;
import java.awt.Event;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.ListModel;
import javax.swing.UIManager;

public class Version1_Gui extends javax.swing.JFrame {

  
    private final ArrayList<Issue> list = new ArrayList<>();
   
    int ok = 0;
    int k = 0;

    /**
     * Creates new form NewJFrame
     */
    public Version1_Gui() {
        
try {
    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
            UIManager.setLookAndFeel(info.getClassName());
            break;
        }
    }
} catch (Exception e) {
    // If Nimbus is not available, you can set the GUI to another look and feel.
}

        initComponents();
        
        readData();
      
    }
    

    private ListModel<Issue> getListFromAlist(List<Issue> aList) {
        DefaultListModel<Issue> result = new DefaultListModel<>();

        aList.stream().forEach((iss) -> {
            result.addElement(iss);
        });
        return result;
    }


public  void select()throws SQLException,ClassNotFoundException{
    Connection conn=DBConnector.getConnection();
        
        String sqlRead="SELECT * FROM issue;";
        
        
        Statement stmt=conn.createStatement();
        stmt.execute(sqlRead);
        ResultSet rs=stmt.getResultSet();
      
        
        
        while(rs.next()){
            
            Issue  issue=new Issue();
            issue.setId(rs.getString("idissue"));
            issue.setSubject(rs.getString("subject"));
            issue.setPriority(rs.getString("priority"));
            issue.setStatus(rs.getString("status"));
            issue.setComment(rs.getString("comment"));
            list.add(issue);
            
           
          jList1.setModel(getListFromAlist(list));  
        }
        
}
    public void insert(Issue issue)throws ClassNotFoundException{

        try {

            Connection conn = DBConnector.getConnection();

            

            String sql = "INSERT INTO issue (idissue,subject,priority,status,comment) VALUES (?,?,?,?,?);";
          
            
               
                PreparedStatement pstmt = conn.prepareStatement(sql);
                //issue.getSubject();
                pstmt.setString(1,issue.getId());
                pstmt.setString(2, issue.getSubject());
                pstmt.setString(3, issue.getPriority());
                pstmt.setString(4, issue.getStatus());
                pstmt.setString(5, issue.getComment());
                pstmt.execute();
             

        } catch (SQLException ex) {

            Logger.getLogger(Version1_Gui.class.getName()).log(Level.SEVERE, null, ex);

        }

       System.out.println("------------End-----------");

    }
   
    public static void delete(Issue issue)throws ClassNotFoundException{
        try {

            Connection conn = DBConnector.getConnection();

            

            String sql = "DELETE FROM issue WHERE idissue= ?;"; 
          
            
               
                PreparedStatement pstmt = conn.prepareStatement(sql);
                //issue.getSubject();
                pstmt.setString(1,issue.getId());
                
                pstmt.execute();
             

        } catch (SQLException ex) {

            Logger.getLogger(Version1_Gui.class.getName()).log(Level.SEVERE, null, ex);

        }
    }
    public  void update(Issue issue)throws ClassNotFoundException{
        
        try {

            Connection conn = DBConnector.getConnection();

            

            String sql = "UPDATE issue SET subject= ?,priority= ?,status= ?,comment= ?"+" WHERE idissue= ?;";
          
            
               
                PreparedStatement pstmt = conn.prepareStatement(sql);
               
                pstmt.setString(5, issue.getId());
                pstmt.setString(1, jTextField1.getText());
                pstmt.setString(2, jComboBox1.getSelectedItem().toString());
                pstmt.setString(3, jComboBox2.getSelectedItem().toString());
                pstmt.setString(4, jTextArea1.getText());
                
                pstmt.execute();
             

        } catch (SQLException ex) {

            Logger.getLogger(Version1_Gui.class.getName()).log(Level.SEVERE, null, ex);

        }

       System.out.println("------------End-----------");
        
    }

    private void readData(){
        if (ok != 0) {
            DefaultListModel listModel = (DefaultListModel) jList1.getModel();
            listModel.removeAllElements();
        }
        try {
            select();

        } catch (SQLException ex) {
            Logger.getLogger(Version1_Gui.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Version1_Gui.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ok = 1;

                              
    }
   

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jButton1 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        msg = new javax.swing.JLabel();
        id = new javax.swing.JLabel();
        ID = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);

        jLabel1.setText("Subject");

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Priority");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "LowPriority", "MediumPriority", "HighPriority" }));

        jLabel3.setText("Status");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NewIssue", "InProgress", "Done" }));

        jLabel4.setText("Comment");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(jList1);

        jButton1.setForeground(new java.awt.Color(0, 0, 51));
        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton5.setText("Add");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton2.setText("Update");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        msg.setForeground(new java.awt.Color(255, 0, 0));

        ID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IDActionPerformed(evt);
            }
        });

        jLabel5.setText("Id");

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(msg, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(117, 117, 117)
                                .addComponent(id)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(18, 18, 18)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(14, 14, 14)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel5))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
                                                .addComponent(jLabel3))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(ID, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel1)))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(63, 63, 63)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton2)))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(33, 33, 33))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(id))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(196, 196, 196)
                        .addComponent(msg)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(41, 41, 41))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton5)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(ID)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2)
                    .addComponent(jLabel2))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged

        
        Issue iss = (Issue) jList1.getSelectedValue();
        if (iss != null) {
            ID.setText(iss.getId());
            jTextField1.setText(iss.getSubject());
            jComboBox2.setSelectedItem(iss.getStatus());
            jComboBox1.setSelectedItem(iss.getPriority());
            jTextArea1.setText(iss.getComment());
        }


    }//GEN-LAST:event_jList1ValueChanged

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
     

            msg.setText("");
            String idiss=ID.getText();
            String subject = jTextField1.getText();
            String priority = jComboBox1.getSelectedItem().toString();
            String status = jComboBox2.getSelectedItem().toString();
            String comment = jTextArea1.getText();
            Issue is = new Issue(idiss,subject, priority, status, comment);
            if(subject.isEmpty() || comment.isEmpty()){
            msg.setText ("please fill subject/comment !");
            }
            else{
            list.add(is);
            jList1.setModel(getListFromAlist(list));
                try {
                    insert(is);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Version1_Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
         
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

            int selectedIndex = jList1.getSelectedIndex();
            
            
            Issue iss = (Issue) jList1.getSelectedValue();
           
            if (iss != null ) {
            list.remove(selectedIndex);
            
            jList1.setModel(getListFromAlist(list));
                try {
                    delete(iss);
                    
                  
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Version1_Gui.class.getName()).log(Level.SEVERE, null, ex);
                }
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        int selectedIndex = jList1.getSelectedIndex();
        String idiss= ID.getText();
        String subject = jTextField1.getText();
        String priority = jComboBox1.getSelectedItem().toString();
        String status = jComboBox2.getSelectedItem().toString();
        String comment = jTextArea1.getText();
        Issue iss = new Issue(idiss,subject, priority, status, comment);
        try {
            update(iss);
            System.out.println("intra in update");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Version1_Gui.class.getName()).log(Level.SEVERE, null, ex);
        }
        list.remove(selectedIndex);
        list.add(selectedIndex, iss);
        
        jList1.setModel(getListFromAlist(list));
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void IDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IDActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IDActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Version1_Gui.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(() -> {
            new Version1_Gui().setVisible(true);
        }
       
        );
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField ID;
    private javax.swing.JLabel id;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton5;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JList jList1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JLabel msg;
    // End of variables declaration//GEN-END:variables
}
